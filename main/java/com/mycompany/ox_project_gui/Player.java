/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.ox_project_gui;

/**
 *
 * @author KHANOMMECOM
 */
public class Player {
    private char symbol;
    private int win,lose,draw;

    public Player(char symbol) {
        this.symbol = symbol;
    }

    
    public char getSymbol() {
        return symbol;
    }

    public int getWin() {
        return win;
    }

    public int getLose() {
        return lose;
    }

    public int getDraw() {
        return draw;
    }

    public void setSymbol(char symbol) {
        this.symbol = symbol;
    }

    public void win() {
        this.win++;
    }

    public void loss() {
        this.lose++;
    }

    public void draw() {
        this.draw ++;
    }

    @Override
    public String toString() {
        return "Player{" + "symbol=" + symbol + ", win=" + win + ", lose=" + lose + ", draw=" + draw + '}';
    }
    
}
